import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from time import sleep

@pytest.mark.functional
class TestClickActionsMenu:

    xpaths = [
        '//a[contains(.,"Alerts")]',
        '//button[contains(.,"Alert")]',
        '//button[contains(.,"Confirm")]'
    ]

    def setup_method(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://qa-automation-practice.netlify.app/")
        self.driver.maximize_window()

    def teardown_method(self):
        self.driver.quit()

    def test_Alert_tab(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(5)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")

    def test_Alert_button(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(5)
            alert_button = self.driver.find_element(By.XPATH, self.xpaths[1])
            alert_button.click()
            sleep(2)
            alert_button.send_keys(Keys.ENTER)
            sleep(3)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")

    def test_Alert_confirmButton(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(5)
            alert_button = self.driver.find_element(By.XPATH, self.xpaths[2])
            alert_button.click()
            sleep(2)
            alert_button.send_keys(Keys.ENTER)
            sleep(3)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")


if __name__ == "__main__":
    pytest.main([__file__])
