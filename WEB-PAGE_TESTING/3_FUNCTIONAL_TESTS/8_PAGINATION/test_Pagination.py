import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from time import sleep

@pytest.mark.functional
class TestClickActionsMenu:

    xpaths = [
            '//a[contains(.,"Pagination")]',
            '//a[contains(@onclick,"showPageNumber(1)")]',
            '//div[contains(@id,"pageResult")][contains(.,"You clicked page no. 1")]',
            '//a[contains(@onclick,"showPageNumber(2)")]',
            '//div[contains(@id,"pageResult")][contains(.,"You clicked page no. 2")]',
            '//a[contains(@onclick,"showPageNumber(3)")]',
            '//div[contains(@id,"pageResult")][contains(.,"You clicked page no. 3")]',
            '//a[contains(@onclick,"showPageNumber(4)")]',
            '//div[contains(@id,"pageResult")][contains(.,"You clicked the "Next" button")]',
            ]

    def setup_method(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://qa-automation-practice.netlify.app/")
        self.driver.maximize_window()

    def teardown_method(self):
        self.driver.quit()

    def test_Pagination_tab(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(5)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")

    def test_Pagination_Page1(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(3)

            page1 = self.driver.find_element(By.XPATH, self.xpaths[1])
            page1.click()
            sleep(3)
            self.driver.find_element(By.XPATH,self.xpaths[2]).is_displayed()
            sleep(2)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")

    def test_Pagination_Page2(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(3)

            page2 = self.driver.find_element(By.XPATH, self.xpaths[3])
            page2.click()
            sleep(3)
            self.driver.find_element(By.XPATH, self.xpaths[4]).is_displayed()
            sleep(2)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")

    def test_Pagination_Page3(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(3)

            page3 = self.driver.find_element(By.XPATH, self.xpaths[5])
            page3.click()
            sleep(3)
            self.driver.find_element(By.XPATH, self.xpaths[6]).is_displayed()
            sleep(2)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")

    def test_Pagination_Page4(self):
        try:
            alert_tab = self.driver.find_element(By.XPATH, self.xpaths[0])
            alert_tab.click()
            sleep(3)

            page4 = self.driver.find_element(By.XPATH, self.xpaths[7])
            page4.click()
            sleep(3)
            self.driver.find_element(By.XPATH, self.xpaths[8]).is_displayed()
            sleep(2)

        except NameError as e:
            print(f"An unexpected error occurred: {e}")

        except Exception as e:
            print(f"An unexpected error occurred: {e}")



if __name__ == "__main__":
    pytest.main([__file__])
